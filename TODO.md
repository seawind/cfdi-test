Método Stamp

- Cuando se timbra correctamente, el formato de fecha devuelto es:
2017-07-08T22:59:13
- Cuando el documento ya esta timbrado (307), el formato es:
2017-07-08 22:59:13


Método Quick_Stamp

* Mismo caso de las fechas


Método Query_Pending

* La fecha la devuelve como: date '2017-07-08 23:42:40.482047'
* El XML lo devuelve sin escapar, los métodos anteriores si lo hacen.
* Si el UUID no existe, devuelve el error: local variable 'invoice'
referenced before assignment


Método get_xml

* Cuando el UUID no existe, el servidor; o cierra la conexión: Remote end
closed connection without response o da el error: Unknown fault occured


Método get_receipt

* El acuse lo devuelve sin escapar


#~ openssl pkcs12 -export -in finkok.cer.pem -inkey finkok.pem -name "My Cert" -out finkok.p12
#~ xmlsec1 --sign --output doc-signed-x509.xml --pkcs12 finkok.p12 --pwd '12345678a' cancel.xml
