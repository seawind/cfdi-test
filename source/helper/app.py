#!/usr/bin/env python3

from logbook import Logger

from . import util
from .cfdi_xml import CFDI
from .pac import get_status_sat
from .configpac import PAC
if PAC == 'ecodex':
    from .pac import Ecodex as PAC
elif PAC == 'finkok':
    from .pac import Finkok as PAC

from settings import DELETE_SOURCE, DEBUG


log = Logger('APP')


def generar_xml(origen, destino, format_files):
    msg = util.validate_folder(destino)
    if msg:
        return msg

    documentos = util.get_files(origen, format_files)
    if not documentos:
        msg = 'Sin datos nuevos para generar XML en: {}'.format(origen)
        return msg

    i = 0
    for doc in documentos:
        msg = 'Generando el documento: {}'.format(doc)
        log.debug(msg)

        datos = util.load_data(doc, format_files)

        if datos is None:
            msg = 'Documento {} no válido: {}'.format(format_files.upper(), doc)
            log.debug(msg)
            continue

        cfdi = CFDI()
        xml = cfdi.get_xml(datos)
        if not xml:
            log.debug(cfdi.error)
            continue

        _, _, nombre, _ = util.get_path_info(doc)
        ruta_xml = util.join(destino, '{}.xml'.format(nombre))
        if util.save_file(ruta_xml, xml):
            if DELETE_SOURCE and not DEBUG:
                util.kill(doc)
            msg = 'Documento generado: {}'.format(ruta_xml)
            log.debug(msg)
            i += 1

    return '{} documentos generados en: {}'.format(i, destino)


def sellar_xml(origen, ruta_cer, ruta_xslt, pass_key, cert_num):
    documentos = util.get_files(origen)
    if not documentos:
        msg = 'Sin documentos XMLs por sellar en: {}'.format(origen)
        return msg

    delete, path_pem = util.validate_pem(ruta_cer, pass_key)
    if not path_pem:
        msg = 'No se pudo generar el archivo PEM'
        return msg

    i = 0
    for doc in documentos:
        msg = 'Sellando el documento: {}'.format(doc)
        log.debug(msg)

        path_xlst = util.get_path_xlst(doc, ruta_xslt)
        if not path_xlst:
            msg = 'No se encontró la ruta XLST, verifica el XML: {}'.format(doc)
            log.debug(msg)
            continue

        sello = util.get_sello(doc, path_xlst, path_pem, ruta_cer)
        if not sello:
            msg = 'No se pudo generar el sello del XML: {}'.format(doc)
            log.debug(msg)
            continue

        if util.add_sello(doc, sello, cert_num, ruta_cer):
            msg = 'Documento sellado: {}'.format(doc)
            log.debug(msg)
            i += 1

    if delete:
        util.kill(path_pem)
    return '{} documentos sellados correctamente'.format(i)


def timbrar_xml(origen, destino):
    msg = util.validate_folder(destino)
    if msg:
        return msg

    documentos = util.get_files(origen, 'xml')
    if not documentos:
        msg = 'Sin documentos XML para enviar a timbrar en: {}'.format(origen)
        return msg

    msg = util.validate_folder(destino)
    if msg:
        return msg

    i = 0
    pac = PAC()
    for doc in documentos:
        msg = 'Enviando a timbrar el documento: {}'.format(doc)
        log.debug(msg)
        xml = pac.timbra_xml(doc)
        if not xml:
            log.error(pac.error)
            continue

        _, _, nombre, _ = util.get_path_info(doc)
        ruta_xml = util.join(destino, '{}.xml'.format(nombre))
        if util.save_file(ruta_xml, xml):
            if DELETE_SOURCE:
                util.kill(doc)
            msg = 'Documento timbrado: {}'.format(ruta_xml)
            log.debug(msg)
            i += 1

    return '{} documentos timbrados correctamente'.format(i)


def timbra_one_xml(path):
    msg = 'Enviando a timbrar el documento: {}'.format(path)
    log.debug(msg)
    pac = PAC()
    xml = pac.timbra_xml(path)
    if xml:
        path, _, nombre, _ = util.get_path_info(path)
        ruta_xml = util.join(path, '{}_TIMBRADO.xml'.format(nombre))
        if util.save_file(ruta_xml, xml):
            msg = 'Documento timbrado: {}'.format(ruta_xml)
            log.debug(msg)
    return


def estatus_sat(origen, path=''):
    if path:
        documentos = (path,)
    else:
        documentos = util.get_files(origen, 'xml')
        if not documentos:
            msg = 'Sin documentos XML para enviar a timbrar en: {}'.format(origen)
            return msg

    for doc in documentos:
        msg = 'Obteniendo estatus del SAT del documento: {}'.format(doc)
        log.info(msg)
        result = get_status_sat(doc)
        log.info(result)
    return


def cancela_otros_xml(origen):
    documentos = util.get_files(origen, 'xml')
    if not documentos:
        msg = 'Sin documentos XML para enviar a timbrar en: {}'.format(origen)
        log.info(msg)
        return

    t = len(documentos)
    for i, doc in enumerate(documentos):
        msg = 'Cancelando el documento {} de {}:\n\t{}'.format(i + 1, t, doc)
        log.info(msg)
        pac = PAC()
        result = pac.cancela_otros_xml(doc)
        for r in result:
            print(r)
    return


