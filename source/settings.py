#!/usr/bin/env python3

import sys
import logbook
import os
from logbook import Logger, StreamHandler, RotatingFileHandler
logbook.set_datetime_format("local")

from conf import NAME_TEST, RFC_TEST, CERT_NUM

#~ Si es False, solo muestra los mensajes INFO,
#~ establece en False para usar en produción
DEBUG = True

#~ Establece la ruta al archivo LOG de registros
LOG_PATH = 'cfdi-test.log'
LOG_NAME = 'CFDI'
LOG_LEVEL = 'INFO'

format_string = '[{record.time:%d-%b-%Y %H:%M:%S}] ' \
    '{record.level_name}: ' \
    '{record.channel}: ' \
    '{record.message}'

RotatingFileHandler(
    LOG_PATH,
    backup_count=10,
    max_size=1073741824,
    level=LOG_LEVEL,
    format_string=format_string).push_application()

StreamHandler(
    sys.stdout,
    level=LOG_LEVEL,
    format_string=format_string).push_application()

if DEBUG:
    LOG_LEVEL = 'DEBUG'
    StreamHandler(
        sys.stdout,
        level=LOG_LEVEL,
        format_string=format_string).push_application()

log = Logger(LOG_NAME)

#~ Aumenta el tiempo de espera en segundos, solo si tienes una conexión muy
#~ lenta o inestable
TIMEOUT = 10

PATH_PROYECT = os.path.dirname(__file__)

#~ Directorios de trabajo dentro de FOLDER_DEFAULT
FOLDERS = {
    'datos': 'datos',
    'generados': 'generados',
    'timbrados': 'timbrados',
}
#~ Carpeta de trabajo predeterminada, dejala vacia si usas tus propias rutas
FOLDER_DEFAULT = ''
if DEBUG:
    #~ Carpeta de trabajo predeterminada dentro de HOME del usuario
    FOLDER_DEFAULT = 'cfdi-test'

#~ Directorio con los archivos XSLT para las transformaciones,
#~ no deberías cambiarlo
FOLDER_XSLT = 'xslt'

#~ Nombre del certificado: IMPORTANTE, establece SOLO el nombre, TODOS los
#~ archivos deben de TENER el MISMO NOMBRE, se requiere al menos el CER y PEM
NAME_CER = 'certificado.{}'

#~ Ruta de los certificados, en producción pasalo como parametro del script
PATH_CER = ''
if DEBUG:
    FOLDER_CERTIFICADOS = 'certificados'
    NAME_CER = NAME_TEST
    #~ Si no se establece el archivo PEM, se genera al sellar, debes de tener la
    #~ contraseña de la llave privada en el archivo conf.py para poder generarlo
    PATH_CER = os.path.join(PATH_PROYECT, FOLDER_CERTIFICADOS)

NAME_XLST = 'cadena_{}_{}.xslt'
PATH_XLST = os.path.join(PATH_PROYECT, FOLDER_XSLT, NAME_XLST)

PATH_XSLTPROC = 'xsltproc'
PATH_OPENSSL = 'openssl'

if 'win' in sys.platform:
    PATH_XSLTPROC = os.path.join(PATH_PROYECT, 'bin', 'xsltproc.exe')
    PATH_OPENSSL = os.path.join(PATH_PROYECT, 'bin', 'openssl.exe')

DELETE_SOURCE = True

#~ El tipo de TXT que se procesa, es la forma de personalizar cada tipo de TXT
#~ que genera cada ERP
TYPE_TXT = 1
