#!/usr/bin/env python3
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

"""Entorno de pruebas para CFDI
"""

import click

from helper.util import join, get_home_user, validate_folder
from helper.app import generar_xml, sellar_xml, timbrar_xml, timbra_one_xml, \
    estatus_sat, cancela_otros_xml
from settings import log, DEBUG, FOLDER_DEFAULT, FOLDERS, PATH_CER, PATH_XLST
from conf import CERT_NUM, FORMAT_FILES, PASS_KEY


def folder_de_trabajo(ctx, param, value):
    if value == FOLDER_DEFAULT:
        if DEBUG:
            path = join(get_home_user(), value)
        else:
            path = FOLDER_DEFAULT
    else:
        path = value

    msg = validate_folder(path)
    if msg:
        raise click.ClickException(msg)
    return path


def _validate_folder(ctx, param, value):
    if not value:
        return ''

    msg = validate_folder(value)
    if msg:
        raise click.ClickException(msg)
    return value


help_folder = 'Directorio de trabajo, donde se toman los datos para los ' \
    'documentos y donde se descargan los documentos timbrados'
help_foldere = 'Directorio de entrada, documentos fuente'
help_folders = 'Directorio de salida, documentos generados'
help_foldert = 'Directorio para documentos timbrados'
help_folderc = 'Directorio de los certificados'
help_generar = 'Toma los datos en la carpeta predeterminada: {} y genera su ' \
    'XML. El formato del archivo es el indicado en settings y cada archivo ' \
    'se considera un documento'.format(FOLDERS['datos'].upper())
help_sellar = 'Toma todos los documentos en la carpeta predeterminada: {} y ' \
    'los sella con el certificado configurado'.format(FOLDERS['generados'].upper())
help_timbrar = 'Toma todos los documentos en la carpeta predeterminada: {} y ' \
    'los manda timbrar con el PAC'.format(FOLDERS['generados'].upper())
help_estatus = 'Verifica el estatus del CFDI directamente en el SAT'
help_xml = 'Ruta al CFDI (XML) a timbrar o a verificar en el SAT'
CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-f', '--folder',
    default=FOLDER_DEFAULT, help=help_folder, callback=folder_de_trabajo)
@click.option('-fe', '--folder-entrada',
    default='', help=help_foldere, callback=_validate_folder)
@click.option('-fs', '--folder-salida',
    default='', help=help_folders, callback=_validate_folder)
@click.option('-ft', '--folder-timbrados',
    default='', help=help_foldert, callback=_validate_folder)
@click.option('-fc', '--folder-certificados',
    default='', help=help_folderc, callback=_validate_folder)
@click.option('-g', '--generar',
    is_flag=True, default=False, help=help_generar)
@click.option('-s', '--sellar',
    is_flag=True, default=False, help=help_sellar)
@click.option('-t', '--timbrar',
    is_flag=True, default=False, help=help_timbrar)
@click.option('-e', '--estatus',
    is_flag=True, default=False, help=help_estatus)
@click.option('-x', '--xml',
    type=click.Path(exists=True), help=help_xml)
@click.option('-co', '--cancela-otros', is_flag=True, default=False)
def main(folder, folder_entrada, folder_salida, folder_timbrados,
    folder_certificados, generar, sellar, timbrar, estatus, xml, cancela_otros):

    if cancela_otros:
        origen = folder_timbrados or join(folder, FOLDERS['timbrados'])
        cancela_otros_xml(origen)
        return

    if not generar and not sellar and not timbrar and not estatus:
        msg = 'Se requiere al menos un parametro: -g, -s, -t o -e'
        raise click.ClickException(msg)

    path_cer = folder_certificados
    if DEBUG:
        path_cer = PATH_CER
    else:
        if not folder_certificados:
            msg = 'Se requiere el folder con los certificados: -fc'
            raise click.ClickException(msg)

    if generar:
        origen = folder_entrada or join(folder, FOLDERS['datos'])
        destino = folder_salida or join(folder, FOLDERS['generados'])
        result = generar_xml(origen, destino, FORMAT_FILES)
        log.info(result)
    if sellar:
        origen = folder_salida or join(folder, FOLDERS['generados'])
        result = sellar_xml(origen, path_cer, PATH_XLST, PASS_KEY, CERT_NUM)
        log.info(result)
    if timbrar:
        if xml:
            timbra_one_xml(xml)
        else:
            origen = folder_salida or join(folder, FOLDERS['generados'])
            destino = folder_timbrados or join(folder, FOLDERS['timbrados'])
            result = timbrar_xml(origen, destino)
            log.info(result)
    if estatus:
        origen = folder_timbrados or join(folder, FOLDERS['timbrados'])
        estatus_sat(origen, xml)
    return


if __name__ == '__main__':
    main()

