## Como colaborar en este proyecto

* Clona y prueba
* Genera documentos JSON, TXT de prueba, sobre todo los especiales del SAT
* Agrega soporte para más complementos del SAT
* Agrega soporte para otros PACs
* Apoya [nuestras actividades](https://universolibre.org/hacemos/)
