# Lista de cambios

Para una lista completa visita el [repositorio del proyecto](https://gitlab.com/mauriciobaeza/cfdi-test/activity).


## v 1.0.2
---
* Error al mover los XSI

## v 1.0.1
---
* Error al no tener comercio exterior

## v 1.0.0
---
* Timbrando con complemento de comercio exterior

## v 0.5.0
---
* Se agrega el soporte para CFDI 3.3 y para timbrar con Finkok

## v 0.0.1
---
* Versión inicial
