# Texto a CFDI

## Scripts para crear y validar documentos electrónicos del SAT (CFDIs).

### Mira el [wiki](https://gitlab.com/mauriciobaeza/cfdi-test/wikis/home) para los detalles de uso.

### Ayudanos a ayudar a otros, conoce [nuestras actividades](http://universolibre.org/hacemos/).
